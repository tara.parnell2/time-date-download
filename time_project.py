import matplotlib.pyplot as plt
import datetime
from astropy import units as u
from pathlib import Path

import sunpy.map
from sunpy.net import Fido, attrs as a

#Define empty variables to use later
date = 0
time = 0
date_time = 0

#Functions to prompt the desired date and time inputs
#If the input is not recognised by datetime, prompt runs again
def get_date(date_variable):
    date_input = input("Please input date in dd-mm-yyyy format:")
    try:
        date_variable = datetime.date(int(date_input[6:]), int(date_input[3:5]), int(date_input[0:2]))
        return date_variable
    except ValueError or TypeError:
        print("Format not recognised.")
        return get_date(date_variable)
#The following line runs the function and copies value of internal variable
date = get_date(date)


def get_time(time_variable):
    time_input = input("Please input time in hh-mm format:")
    try:
        time_variable = datetime.time(int(time_input[:2]), int(time_input[3:]))
        return time_variable
    except ValueError:
        try:
            time_variable = datetime.time(int(time_input[:2]), int(time_input[3:5]), int(time_input[6:]))
            return time_variable
        except ValueError:
            print("Format not recognised.")
            return get_time(time_variable)

time = get_time(time)
date_time = datetime.datetime.combine(date, time)
print(date_time)

#List the wavelengths for AIA to get data for
wavelength = [131, 171]  # , 211, 304, 335]

#Create two folders to download the data to
#The outer folder is placed in the C drive and named after the desired date
#A second folder is placed inside which states the time of the data
date_folder = Path.cwd() / str('downloads') / str(date)
try:
    date_folder.mkdir(parents=True, exist_ok=False)
except FileExistsError:
    print('Folder already exists.')

time_folder = date_folder / str(time.strftime('%H')+'-'+time.strftime('%M'))
try:
    time_folder.mkdir(parents=True, exist_ok=False)
except FileExistsError:
    print('Folder already exists.'
          'Data may already exist.')
    proceed = input('Do you wish to proceed? (Y/N) ')
    if proceed == 'Y' or proceed == 'y':
        print('Proceeding')
    else:
        quit()


#Creating the AIA image
#Use date and time variables with strftime to give values for Fido
AIA_time = date_time - datetime.timedelta(seconds = 12)
print(AIA_time)
for l in wavelength:
    res = Fido.search(a.Time(AIA_time.strftime('%Y')+'-'+AIA_time.strftime('%m')+'-'+AIA_time.strftime('%d')+' '+AIA_time.strftime('%H')+':'+AIA_time.strftime('%M')+':'+AIA_time.strftime('%S'),
                             date.strftime('%Y')+'-'+date.strftime('%m')+'-'+date.strftime('%d')+' '+time.strftime('%H')+':'+time.strftime('%M')+':'+time.strftime('%S')),
                      a.Instrument("AIA"),
                      a.Wavelength(l*u.angstrom))

    print(res)

#Download the first data entry for each wavelength and download to time folder
    Fido.fetch(res[0][0], path=time_folder)

for file in time_folder.iterdir():
    print(file.name)

#Plot a colour map of each downloaded file
for file in time_folder.iterdir():
    mapa = sunpy.map.Map(time_folder / str(file))
    try:
        mapb = plt.get_cmap('sdoaia' + str(file.name)[9:12])
    except ValueError:
        mapb = plt.get_cmap('sdoaia' + str(file.name)[20:23])

    fig = plt.figure()
    mapa.plot(cmap=mapb)
    mapa.draw_limb()
    mapa.draw_contours([10, 20, 30, 40, 50, 60, 70, 80, 90] * u.percent)
    plt.colorbar()
    new_file = file.name.replace('fits', 'png')
    plt.savefig(time_folder / str(new_file))

#Download GOES data
#Use date and time variables with strftime to give values for Fido
#GOES_time = date_time - datetime(0000, 0, 1, 0, 0, 0)

#result = Fido.search(a.Time(GOES_time.strftime('%Y')+'-'+GOES_time.strftime('%m')+'-'+GOES_time.strftime('%D')+' '+GOES_time.strftime('%H')+':'+GOES_time.strftime('%M')+':'+GOES_time.strftime('%S'),
#                            date.strftime('%Y')+'-'+date.strftime('%m')+'-'+date.strftime('%d')+' '+time.strftime('%H')+':'+time.strftime('%M')+':'+time.strftime('%S')),
#                     a.Instrument("XRS"))

#print(result)

#Download the first data entry and download to time folder
#Fido.fetch(result[0][0], path=time_folder)
